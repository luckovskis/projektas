<!DOCTYPE html>
<html>
<head>
	<title>Standart</title>
	<?php include 'headerlink.html'; ?>
	<link rel="stylesheet" type="text/css" href="css/stylestd.css">
	
</head>
<body>
<?php 
include 'header.html';
//$tipas = null ;
if(isset($_GET['tipas']))
{
	$tipas = $_GET['tipas'];
}
else
{
	$tipas = null ;
}
// echo "tipas yra: $tipas";
 ?>
	<main>
		<ul>
			<li>
				<ul class="row container center-align">
					<li class="col s12 m6 l4 hide-on-med-and-down "><img class="imgico " src="images/icobmw.png"></li>
					<li class="col s12 m6 l4 hide-on-large-only "><a href='brand.php?tipas=<?php echo "$tipas"; ?>&brand=bmw'><img class="imgico " src="images/icobmw.png"></a></li>
					<li class="col s12 m6 l4">
						<ul class="left-align">
							<li><h6 class="text-bold">BMW</h6></li>
							<li><p>Oficialus puslapis <a href="http://www.bmw-motorrad.com/com/index.html">čia</a></p></li>
							<li><p>.</p></li>
							<li><p>.</p></li>
						</ul>
					</li>
					<li class="col s12 m6 l4">
						<a href="brand.php?tipas=<?php echo $tipas ?>&brand=bmw" class="waves-effect waves-light btn but hide-on-med-and-down">Peržiūra</a>
					</li>
				</ul>
				
			</li>
			<li class="divider"></li>
			<li>
				<ul class="row container center-align">
					<li class="col s12 m6 l4 hide-on-med-and-down "><img class="imgico " src="images/hondaico.jpg"></li>
					<li class="col s12 m6 l4 hide-on-large-only "><a href="brand.php?tipas=<?php echo $tipas ?>&brand=honda"><img class="imgico " src="images/hondaico.jpg"></a></li>
					<li class="col s12 m6 l4">
						<ul class="left-align">
							<li><h6 class="text-bold">Honda</h6></li>
							<li><p>Oficialus puslapis <a href="http://powersports.honda.com">čia</a></p></li>
							<li><p>.</p></li>
							<li><p>.</p></li>
						</ul>
					</li>
					<li class="col s12 m6 l4">
						<a href="brand.php?tipas=<?php echo $tipas ?>&brand=honda" class="waves-effect waves-light btn but hide-on-med-and-down">Peržiūra</a>
					</li>
				</ul>
			</li>
			<li class="divider"></li>
			<li>
				<ul class="row container center-align but">
					<li class="col s12 m6 l4 hide-on-med-and-down "><img class="imgico " src="images/suzukiico.png"></li>
					<li class="col s12 m6 l4 hide-on-large-only "><a href="brand.php?tipas=<?php echo $tipas ?>&brand=suzuki"><img class="imgico " src="images/suzukiico.png"></a></li>
					<li class="col s12 m6 l4">
						<ul class="left-align">
							<li><h6 class="text-bold">Suzuki</h6></li>
							<li><p>Oficialus puslapis <a href="http://www.suzukimoto.lt">čia</a></p></li>
							<li><p>.</p></li>
							<li><p>.</p></li>
						</ul>
					</li>
					<li class="col s12 m6 l4">
						<a href="brand.php?tipas=<?php echo $tipas ?>&brand=suzuki" class="waves-effect waves-light btn but hide-on-med-and-down">Peržiūra</a>
					</li>
				</ul>

			</li>
			<li class="divider"></li>
		</ul>
		
	</main>
	<?php 
include 'footer.html';
	 ?>

</body>
</html>