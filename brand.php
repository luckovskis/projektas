<!DOCTYPE html>
<html>
<head>
	<title>Gamintojai</title>
	<?php include 'headerlink.html'; ?>
	<link rel="stylesheet" type="text/css" href="css/stylebrand.css">
	<script src="js/scriptbrand.js"></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script src="http://canvasjs.com/assets/script/canvasjs.min.js"></script>
</head>
<body>
	<?php include 'header.html'; ?>
	<?php 

	$brandas1 = null;
	$brandas2 = null;
	$brandas3 = null;
	$tipas1 = null;
	$tipas2 = null;
	$tipas3 = null;
	$t1priskirtas = false;
	$brandai = array("bmw", "honda", "suzuki");
	$tipai = array("standart","sport","touring");
	if(isset($_GET['tipas']) && isset($_GET['brand']))
	{
		$tipas = $_GET['tipas'];
		$brandas = $_GET['brand'];
		
	}
	else
	{
		$tipas = 'standart' ;
		$brandas = 'bmw';
		
	}
	if($brandas=="")$brandas=$brandai[0];
	if($tipas!="")
	{
		for ($i =0 ; $i<3 ; $i++ ) {
			if($brandas == $brandai[ $i ])
			{
				$brandas1 = $brandai[ $i ];
				$t1priskirtas = true;

			}
			else
			{
				$brandas2 = $brandai[$i];
				$brandas3 = $brandai[(($i+1)>2?0:($i+1))];
				if($t1priskirtas)$i=3;

			}
		}
		$tipas1 = $tipas;
		$tipas2 = $tipas;
		$tipas3 = $tipas;
	}
	else
	{
		$tipas1 = $tipai[0];
		$tipas2 = $tipai[1];
		$tipas3 = $tipai[2];
		$brandas1 = $brandas;
		$brandas2 = $brandas;
		$brandas3 = $brandas;
	}
	// if($brandas1=="")$brandas1=$brandai[1];
	// if($tipas=="")$tipas="standart";
// echo $brandas1.$tipas;

  function get_content($addr)
 {

    $servername = "85.10.205.173:3306";
    $username = "valentas";
    $password = "valentas";
    $dbname = "vilniuscodingval";
    $table = "my_table";
    

// Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
   // echo 'check ok<br>';
//print_r($conn);

    if ($conn-> connect_error) {
        die("Connection failed: " . $conn->connect_error);

    }
    else
    {
     //echo "conection success";
 }


 	//echo "addr:$addr";
 $sql = "SELECT * FROM"." $table"." where name='$addr'";
 // echo "sqlas:".$sql;
 $result = mysqli_query($conn, $sql);
 // echo "<br>Result ";
 // // print_r($result);
 if (mysqli_num_rows($result) > 0)
 {
    //var_dump($result);
    $row = mysqli_fetch_assoc($result);
    return ($row['content']);
   
   
}
}
	?>

	<br>
	<div >
		<section>
			<!-- <div id="pav1" style="background: url(images/<?php echo $brandas1.$tipas ?>.jpg);background-size: 700px 500px;background-repeat: repeat;"> -->
				<img id="pav1" class="z-depth-3" src="images/<?php echo $brandas1.$tipas1 ?>.jpg">
				<p id="pavp1"><?php echo get_content($brandas1.$tipas1.".txt") ;?></p>
			<!-- </div> -->
		</section>

		<script type="text/javascript">
			google.charts.load('current', {'packages':['corechart']});
			google.charts.setOnLoadCallback(drawChart);
			function drawChart() {

				var data = google.visualization.arrayToDataTable([
					['Task', 'Hours per Day'],
					['BMW',     56],
					['Honda',      32],
					['Suzuki',    12]
					]);

				var options = {
					title: '2015 metu motociklu pardavimai'
				};

				var chart = new google.visualization.PieChart(document.getElementById('piechart'));
				chart.draw(data, options);
				var data = google.visualization.arrayToDataTable([
					['Task', 'Hours per Day'],
					['BMW',     12],
					['Honda',      76],
					['Suzuki',    43]
					]);

				var options = {
					title: '2016 metu motociklu pardavimai'
				};
				var chart = new google.visualization.PieChart(document.getElementById('piechart2'));
				chart.draw(data, options);
				var data = google.visualization.arrayToDataTable([
					['Task', 'Hours per Day'],
					['BMW',     40],
					['Honda',      22],
					['Suzuki',    15]
					]);

				var options = {
					title: '2017 metu motociklu pardavimai'
				};
				var chart = new google.visualization.PieChart(document.getElementById('piechart3'));
				chart.draw(data, options);
			}
		</script>
		
    </head>
		
		<br>
		<ul id="ulas" class="row">
			<li class="col s10 l6">
				<ul class="row">
					<li class="col s10 l6">
						<img id="id1" class="pav2 z-depth-3" src="images/<?php echo $brandas2.$tipas2 ;?>.jpg">
					</li>
					<li class="col s10 l6">
						<p id="pavp2" class="truncate"><?php echo get_content($brandas2.$tipas2.".txt") ;?></p>
					</li>
				</ul>
			</li>
			<li class="col s10 l6">
				<ul class="row">
					<li class="col s10 l6">
						<img id="id2" class="pav2 z-depth-3" src="images/<?php echo $brandas3.$tipas3 ; ?>.jpg">
					</li>
					<li class="col s10 l6">
						<p id="pavp3" class="truncate"><?php echo get_content($brandas3.$tipas3.".txt") ;?></p>
					</li>
				</ul>
			</li>

		</ul>
		
		<ul class="collapsible" data-collapsible="accordion">
		    <li>
				<div class="collapsible-header z-depth-3"><i class="material-icons">trending_up</i>Statistika</div>
				
				<div class=" collapsible-body">
					<ul class="row">
						<li class="col s12 m4 l4">
							<div id="piechart" style="width: 450px; height: 250px;"></div>
						</li>
						<li class="col s12 m4 l4">
							<div id="piechart2" style="width: 450px; height: 250px;"></div>
							
						</li>
						<li class="col s12 m4 l4">
							<div id="piechart3" style="width: 450px; height: 250px;"></div>
						</li>
					</ul>
				</div>
		    </li>		    
		</ul>
		
	</div>

	<br>
	<div id="footeris">
		<br>

		<?php 
		include 'footer.html';
		?>

	</div>
</body>
</html>