<!DOCTYPE html>
<html>
<head>
	<title>Title</title>
	<?php include 'headerlink.html'; ?>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script src="js/jssor.slider-22.0.15.min.js" type="text/javascript" data-library="jssor.slider" data-version="22.0.15"></script>
	<script src="js/script.js"></script>
	<?php 
	$dir          = 'images';
	$ImagesA = Get_ImagesToFolder($dir);
	$a = 0;
		//print_r($ImagesA);

	function Get_ImagesToFolder($dir){
		$ImagesArray = [];
		$file_display = [ 'jpg', 'jpeg', 'png', 'gif' ];


		if (file_exists($dir) == false) {
			return ["Directory \'', $dir, '\' not found!"];
		} 
		else {
			$dir_contents = scandir($dir);
			foreach ($dir_contents as $file) {
				$file_type = pathinfo($file, PATHINFO_EXTENSION);
				if (in_array($file_type, $file_display) == true) {
					$ImagesArray[] = $file;

				}
			}

			return $ImagesArray;
		}
	}


	?>
	<script type="text/javascript">
		jssor_1_slider_init = function() {

			var jssor_1_SlideshowTransitions = [

			{$Duration:1000,x:-0.2,$Delay:40,$Cols:12,$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationStraight,$Assembly:260,$Easing:{$Left:$Jease$.$InOutExpo,$Opacity:$Jease$.$InOutQuad},$Opacity:2,$Outside:true,$Round:{$Top:0.5}},

			];

			var jssor_1_options = {
				$AutoPlay: true,
				$SlideshowOptions: {
					$Class: $JssorSlideshowRunner$,
					$Transitions: jssor_1_SlideshowTransitions,
					$TransitionsOrder: 1
				},
				$ArrowNavigatorOptions: {
					$Class: $JssorArrowNavigator$
				},
				$BulletNavigatorOptions: {
					$Class: $JssorBulletNavigator$
				}
			};

			var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

			/*responsive code begin*/
			/*you can remove responsive code if you don't want the slider scales while window resizing*/
			function ScaleSlider() {
				var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
				if (refSize) {
					refSize = Math.min(refSize, 600);
					jssor_1_slider.$ScaleWidth(refSize);
				}
				else {
					window.setTimeout(ScaleSlider, 30);
				}
			}
			ScaleSlider();
			$Jssor$.$AddEvent(window, "load", ScaleSlider);
			$Jssor$.$AddEvent(window, "resize", ScaleSlider);
			$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
			/*responsive code end*/
		};
		

	</script>
	<audio autoplay>

  <source src="load.mp3" type="audio/mpeg">
  </audio>
</head>
<body onload = "startTimer()" >

	<?php 
	include 'header.html';

	?>
	
	<main >


			<!-- <div class="parallax"><img class="responsive-img " id="first" src='<?php echo "$dir/$ImagesA[0]" ;  ?>  '><div class="parallax"> -->

			<div id="jssor_1" class="z-depth-3" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 100%; height: 100%; overflow: hidden; visibility: hidden;">
				<!-- Loading Screen -->
				<div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
					<div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
					<div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
				</div>

				<div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 100%; height: 100%; overflow: hidden;">
				<h1 class="center-align red-text truputis">Truputis apie moto</h1>
					<?php 

					foreach ($ImagesA as $value) {
						echo " <div data-p='112.50' ";
						if($a == 0)echo "style='display:none';";
						echo "><img data-u='image' src='$dir/$value' /> </div>" ;
						$a++;
					} ?>

				</div>
				<!-- Bullet Navigator -->
				<div data-u="navigator" class="jssorb01" style="bottom:16px;right:16px;">
					<div data-u="prototype" style="width:12px;height:12px;"></div>
				</div>
				<!-- Arrow Navigator -->
				<span data-u="arrowleft" class="jssora05l" style="top:0px;left:8px;width:40px;height:40px;" data-autocenter="2"></span>
				<span data-u="arrowright" class="jssora05r" style="top:0px;right:8px;width:40px;height:40px;" data-autocenter="2"></span>
			</div>
			<script type="text/javascript">jssor_1_slider_init();</script>
			<!-- #endregion Jssor Slider End -->
			

		
		<ul class="row">
			<li class=" center-align col s12 l4 ">
				<img class="small z-depth-3" src="images/standard.jpg">
				<h5 class="valign center-align">Standartiniai</h5>
				<a href="type.php?tipas=standart" class="waves-effect waves-light btn z-depth-4">Eiti</a>
				<p>Daugiau apie Standard <a href="https://en.wikipedia.org/wiki/Types_of_motorcycles#Standard">Wiki</a></p>
				<p><a href=""></a></p>
			</li>
			<li class="center-align col s12 l4">
				<img class="small z-depth-3" src="images/sportbike.jpg">
				<h5 class="valign center-align ">Sportiniai</h5>
				<a href="type.php?tipas=sport" class="waves-effect waves-light btn z-depth-4">Eiti</a>
				<p>Daugiau apie Sport bike <a href="https://en.wikipedia.org/wiki/Types_of_motorcycles#Sport_bike">Wiki</a></p>
				<p><a href=""></a></p>
			</li>
			<li class="center-align col s12 l4">
				<img class="small z-depth-3" src="images/cruzier.jpg">
				<h5 class="valign center-align ">Touring'ai</h5>
				<a href="type.php?tipas=touring" class="waves-effect waves-light btn z-depth-4">Eiti</a>
				<p>Daugiau apie Touring <a href="https://en.wikipedia.org/wiki/Types_of_motorcycles#Touring">Wiki</a></p>
				<p><a href=""></a></p>
			</li>
		</ul>
	</main>	
	<footer>
		<?php 
		include 'footer.html';
		?>

	</footer>
</body>
</html>